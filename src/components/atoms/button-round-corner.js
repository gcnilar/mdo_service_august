import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import {Colors} from '_styles';

class ButtonRoundCorner extends Component {
  render() {
    const {text, onPress} = this.props;
    return (
      <TouchableOpacity style={styles.buttonStyle} onPress={() => onPress()}>
        <Text style={styles.textStyle}>{text}</Text>
      </TouchableOpacity>
    );
  }
}

ButtonRoundCorner.propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 20,
    color: Colors.GC_SECONDARY,
    textAlign: 'center',
  },

  buttonStyle: {
    padding: 10,
    backgroundColor: Colors.GC_PRIMARY,
    borderRadius: 5,
  },
});

export default ButtonRoundCorner;
