import {setI18nConfig, translate, RNLocalize} from './translate-utils';

export {setI18nConfig, translate, RNLocalize};
