import React from 'react';
import {HelloWorld, ButtonRoundCorner} from '_atoms';
import {View} from 'react-native';

const App = () => (
  <View>
    <HelloWorld name="Helder Burato Berto" />
    <ButtonRoundCorner
      text="Click Me da"
      onPress={() => {
        alert('Hi there!!!');
      }}
    />
  </View>
);

export default App;
